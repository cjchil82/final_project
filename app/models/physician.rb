class Physician < ActiveRecord::Base
  has_many :appointments
  has_many :patients, through: :appointments
  has_many :diagnostics, through: :appointments
  has_many :time_slots, through:  :appointments
  def if_true
    if self.physician_status == 'true'
      self.physician_status = 'Available'
    end
  end
end
