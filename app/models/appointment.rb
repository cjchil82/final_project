class Appointment < ActiveRecord::Base
  belongs_to :physician
  belongs_to :patient
  belongs_to :time_slot
  belongs_to :diagnostic

  validates_uniqueness_of :time_slot_id, :scope => [:date, :physician_id]
  validates :date, presence: true

  before_create :date_valid
  before_destroy :cant_cancel


  def date_valid
    if self.date < Date.today
      errors.add(:date, "Date must be in the future")
      return false
    else
      if date.saturday? == true or date.sunday? == true
        errors.add(:date, 'selected must be a weekday.')
        return false
      end
    end
  end
  def cant_cancel
    time_to_cancel = self.time_slot.time - Time.now
    if self.date == Date.today and time_to_cancel < 8
      errors.add(:time_slot, "Appointment cannot be canceled less than 8 hours before appointment.")
      return false
    end
  end

end
