json.extract! @appointment, :id, :patient_id, :physician_id, :time_slot_id, :diagnostic_id, :total, :date, :complaint, :note, :created_at, :updated_at
