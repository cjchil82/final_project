json.array!(@diagnostics) do |diagnostic|
  json.extract! diagnostic, :id, :diagnostic_code, :diagnostic_name
  json.url diagnostic_url(diagnostic, format: :json)
end
