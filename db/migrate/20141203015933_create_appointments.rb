class CreateAppointments < ActiveRecord::Migration
  def change
    create_table :appointments do |t|
      t.integer :patient_id
      t.integer :physician_id
      t.integer :time_slot_id
      t.integer :diagnostic_id
      t.float :total
      t.date :date
      t.text :complaint
      t.text :note

      t.timestamps
    end
  end
end
