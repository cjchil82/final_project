class CreateTimeSlots < ActiveRecord::Migration
  def change
    create_table :time_slots do |t|
      t.string :time_name
      t.time :time
      t.boolean :taken

      t.timestamps
    end
  end
end
